import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Geolocation } from '@ionic-native/geolocation';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  latitud;
  longitud;
  
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private geolocation: Geolocation) {
  }

  ionViewDidLoad() {
    this.getPosition();
  }
  getPosition():any{
    this.geolocation.getCurrentPosition()
      .then(result => {
      console.log(result);
      this.mostrarLatLong(result.coords.latitude, result.coords.longitude);
    })
    .catch(error =>{
    console.log(error);
  })
  }
  mostrarLatLong(lat, lng) {
     this.latitud = lat;
     this.longitud = lng;
    console.log('lat' + lat, 'long' +lng)
  }

}
